#!/usr/bin/ruby

require 'progress_bar'

require_relative 'mysql'

mysql = MySql.new

sql = File.read('contacts.sql').split(';')

puts sql.count

@bar = ProgressBar.new sql.count

sql.each do |s|
  mysql.update_contact s
  @bar.increment!
end

puts "Processo finalizado!"